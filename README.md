# WallaMarvel

Steps to run the application:

1. open terminal  
2. run pod install on the project folder

Steps to run the application tests:

1. select the `WallaMarvel` scheme
2. select `iOS 14` for the running device
3. run the tests

Before running the tests make sure you have selected `iOS 14` version as the snapshot tests were recorded based on this OS version.
 
 * This app has been optimized for `Dark Mode`, check it out, it looks very nice. 
