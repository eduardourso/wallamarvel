//
//  Comic.swift
//  Core
//
//  Created by Eduardo Urso on 23.08.19.
//

import Foundation

public struct Media: Codable {
    public let id: Int
    public let title: String
    public let thumbnail: Thumbnail?
}
