//
//  Comics.swift
//  Core
//
//  Created by Eduardo Urso on 23.08.19.
//

import Foundation

public struct MediaList: Decodable {
    public let results: [Media]
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.results = try response.decode([Media].self, forKey: .results)
    }
    
    public init(results: [Media]) {
        self.results = results
    }
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case results = "results"
    }
    
}
