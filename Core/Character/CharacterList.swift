//
//  CharacterList.swift
//  Core
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

public struct CharacterList: Decodable {
    public let results: [Character]
    public let total: Int
    public let offset: Int
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let response = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        self.results = try response.decode([Character].self, forKey: .results)
        self.total = try response.decode(Int.self, forKey: .total)
        self.offset = try response.decode(Int.self, forKey: .offset)
    }
    
    public init(results: [Character], total: Int, offset: Int) {
        self.results = results
        self.total = total
        self.offset = offset
    }
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case results = "results"
        case total
        case offset
    }
    
}
