//
//  Thumbnail.swift
//  Core
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

public struct Thumbnail: Codable {
    public let fileExtension: String
    public let path: String
    
    enum CodingKeys: String, CodingKey {
        case fileExtension = "extension"
        case path = "path"
    }
    
    public func thumbnailURL() -> URL? {
        return URL(string: path + "." + fileExtension)
    }
}
