//
//  Character.swift
//  Core
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

public struct Character: Codable {
    public let id: Int
    public let name: String
    public let description: String
    public let resourceURI: String
    public let thumbnail: Thumbnail
}
