//
//  MarvelCharacterDetailDataSource.swift
//  Core
//
//  Created by Eduardo Urso on 23.08.19.
//

import Foundation
import RxSwift

public protocol CharacterDetailDataSource {
    func fetchComics(for characterID: Int) -> Observable<MediaList>
    func fetchSeries(for characterID: Int) -> Observable<MediaList>
    func fetchEvents(for characterID: Int) -> Observable<MediaList>
}

public class MarvelCharacterDetailDataSource: CharacterDetailDataSource {
    
    private let apiService: APIService
    
    public init(apiService: APIService) {
        self.apiService = apiService
    }
    
    public func fetchComics(for characterID: Int) -> Observable<MediaList> {
        let request = MarvelAPI.comicsForCharacter(id: characterID)
        return apiService.fetch(request: request)
    }
    
    public func fetchSeries(for characterID: Int) -> Observable<MediaList> {
        let request = MarvelAPI.seriesForCharacter(id: characterID)
        return apiService.fetch(request: request)
    }
    
    public func fetchEvents(for characterID: Int) -> Observable<MediaList> {
        let request = MarvelAPI.eventsForCharacter(id: characterID)
        return apiService.fetch(request: request)
    }
}
