//
//  MarvelIndexDataSource.swift
//  Core
//
//  Created by Eduardo Urso on 22.08.19.
//

import Foundation
import RxSwift

public protocol IndexDataSource {
    func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList>
}

public class MarvelIndexDataSource: IndexDataSource {
    
    private let apiService: APIService
    
    public init(apiService: APIService) {
        self.apiService = apiService
    }
    
    public func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList> {
        let request = MarvelAPI.characters(nameStartsWith: searchTerm, offset: offset)
        return apiService.fetch(request: request)
    }
}
