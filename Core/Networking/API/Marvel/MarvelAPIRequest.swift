//
//  MarvelAPI.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

fileprivate struct MarvelAPIConfig {
    static let privatekey = "c2a3d3bc5ec63f7f9cd777f274abdbfff134a20e"
    static let apikey = "a1c5cc45d7ab9ce2fbaa69b7043d45b0"
    static let ts = Date().timeIntervalSince1970.description
    static let hash = "\(ts)\(privatekey)\(apikey)".md5()
}

public extension APIRequest {
    var urlRequest: URLRequest {
        guard let urlString = baseURL.appending(path).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), var urlComponents = URLComponents(string: urlString) else {
            preconditionFailure("Could not build URL components from URL string")
        }

        if let queryItems = queryItems {
            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            preconditionFailure("Could not build URL components from URL string")
        }
        
        var urlRequest = URLRequest(url: url)
        
        if let headers = headers {
            for (headerField, headerValue) in headers {
                urlRequest.setValue(headerValue, forHTTPHeaderField: headerField)
            }
        }
        
        return urlRequest
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var baseURL: String {
        return "https://gateway.marvel.com"
    }
}

enum MarvelAPI: APIRequest {
    
    case characters(nameStartsWith: String?, offset: Int)
    case comicsForCharacter(id: Int)
    case seriesForCharacter(id: Int)
    case eventsForCharacter(id: Int)
    
    var path: String {
        switch self {
        case .characters:
            return "/v1/public/characters"
        case .comicsForCharacter(let id):
            return "/v1/public/characters/\(id)/comics"
        case .seriesForCharacter(let id):
            return "/v1/public/characters/\(id)/series"
        case .eventsForCharacter(let id):
            return "/v1/public/characters/\(id)/events"
        }
    }
    
    var queryItems: [URLQueryItem]? {
        var queryItems = [URLQueryItem(name: "apikey", value: MarvelAPIConfig.apikey),
                          URLQueryItem(name: "ts", value: MarvelAPIConfig.ts),
                          URLQueryItem(name: "hash", value: MarvelAPIConfig.hash)]
        
        switch self {
        case .characters(let nameStartsWith, let offset):
            queryItems.append(URLQueryItem(name: "offset", value: "\(offset)"))
            guard let nameStartsWith = nameStartsWith else { break }
            queryItems.append(URLQueryItem(name: "nameStartsWith", value: nameStartsWith))
        default:
            break
        }
        
        return queryItems
    }
}
