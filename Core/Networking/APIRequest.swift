//
//  APIRequest.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation
import CryptoSwift

public protocol APIRequest {
    var path: String { get }
    var baseURL: String { get }
    var headers: [String: String]? { get }
    var queryItems: [URLQueryItem]? { get }
    
    var urlRequest: URLRequest { get }
}
