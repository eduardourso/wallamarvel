//
//  APIService.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation
import RxSwift
import RxCocoa
import Reachability

public protocol APIService {
    func fetch<T>(request: APIRequest) -> Observable<T> where T: Decodable
}

public class HTTPAPIService: APIService {
    private let urlSessionFactory: URLSessionFactory
    private let reachability = Reachability()
    static let standard = HTTPAPIService(configuration: URLSessionConfigurationFactory.standard())
    
    public init(configuration: URLSessionConfiguration = URLSessionConfigurationFactory.standard()) {
        self.urlSessionFactory = URLSessionFactory(configuration: configuration)
    }
    
    public func fetch<T>(request: APIRequest) -> Observable<T> where T: Decodable {
        return urlSessionFactory.makeSession().rx
            .data(request: request.urlRequest)
            .catchError(transformToNoConnectionErrorIfOffline)
            .catchError(transformToNotFoundErrorIfHttpRequestFailed)
            .compactMap({ try? JSONDecoder().decode(T.self, from: $0) })
    }
    
    private func transformToNoConnectionErrorIfOffline(error: Error) -> Observable<Data> {
        guard !reachability.isReachable() else {
            return Observable.error(error)
        }
        
        let offlineError = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet)
        return Observable.error(offlineError)
    }
    
    private func transformToNotFoundErrorIfHttpRequestFailed(error: Error) -> Observable<Data> {
        guard case RxCocoaURLError.httpRequestFailed(let response, _) = error else {
            return Observable.error(error)
        }
        
        guard response.statusCode == 404 else {
            let genericError = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown)
            return Observable.error(genericError)
        }
        
        let notFoundError = NSError(domain: NSURLErrorDomain, code: NSURLErrorResourceUnavailable)
        return Observable.error(notFoundError)
    }
}
