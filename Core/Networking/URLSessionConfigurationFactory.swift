//
//  URLSessionConfigurationFactory.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

final public class URLSessionConfigurationFactory {
    private static let defaultTimeout = TimeInterval(60)
    private static let defaultDiskCapacity = 20 * 1024 * 1024
    
    static public func standard() -> URLSessionConfiguration {
        return createConfiguration(timeout: defaultTimeout,
                                   cachePolicy: .useProtocolCachePolicy)
    }
    
    private static func createConfiguration(timeout: TimeInterval,
                                            cachePolicy: NSURLRequest.CachePolicy) -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeout
        configuration.requestCachePolicy = cachePolicy
        
        return configuration
    }
}
