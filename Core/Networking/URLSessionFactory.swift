//
//  URLSessionFactory.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation

final class URLSessionFactory {
    private let configuration: URLSessionConfiguration
    
    init(configuration: URLSessionConfiguration) {
        self.configuration = configuration
    }
    
    func makeSession() -> URLSession {
        configuration.requestCachePolicy = .useProtocolCachePolicy
        
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
    }
}
