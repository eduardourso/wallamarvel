//
//  MainNavigator.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import UIKit
import Core

class MainNavigator: NSObject {
    
    fileprivate var navigationController: UINavigationController!
    fileprivate var window: UIWindow?
    private let splitViewController = UISplitViewController()
    
    override init() {
        let indexService = IndexService(indexDataSource: Dependencies.DataSources.indexDataSource)
        super.init()
        
        let viewController = IndexViewController(service: indexService, navigator: self)
        self.navigationController = UINavigationController(rootViewController: viewController)
        splitViewController.viewControllers = [navigationController, navigationController]
        splitViewController.preferredDisplayMode = .allVisible
    }
    
    func toStart(inWindow mainWindow: UIWindow) {
        window = mainWindow
        window?.rootViewController = splitViewController
        window?.makeKeyAndVisible()
    }
}

extension MainNavigator: IndexNavigator {
    func toCharacterDetail(character: Character) {
        let service = CharacterDetailService(dataSource: Dependencies.DataSources.characterDetailDataSource)
        let viewController = CharacterDetailViewController(service: service, character: character)
        splitViewController.showDetailViewController(viewController, sender: nil)
    }
}
