import Foundation
import RxSwift
import RxCocoa

typealias ImageCache = NSCache<NSString, UIImage>

class URLImageView: ImageView {
    private var currentURL: URL?
    private var disposeBag = DisposeBag()
    private let imageCache: ImageCache

    init(sharedImageCache: ImageCache = Dependencies.sharedImageCache) {
        self.imageCache = sharedImageCache
        super.init(frame: .zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(with url: URL?) {
        guard let url = url else {
            setPlaceholder()
            return
        }
        guard url != currentURL else {
            return
        }
        updateView(with: url)
    }

    private func updateView(with url: URL) {
        setPlaceholder()
        currentURL = url
        disposeBag = DisposeBag()

        let key = url.absoluteString as NSString
        if let cached = imageCache.object(forKey: key) {
            updateImage(image: cached)
            return
        }

        let request = URLRequest(url: url)
        return URLSession.shared.rx
            .data(request: request)
            .map(UIImage.init)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] image in
                if let image = image {
                    self?.imageCache.setObject(image, forKey: key)
                    self?.updateImage(image: image)
                }
            }).disposed(by: disposeBag)
    }

    override func reset() {
        super.reset()
        imageView.layer.removeAllAnimations()
        imageView.image = nil
        currentURL = nil
        disposeBag = DisposeBag()
    }

}

class ImageView: UIView {
    var imageView = UIImageView(frame: CGRect.zero)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setPlaceholder(color: UIColor = .darkGray) {
        imageView.image = nil
        imageView.backgroundColor = color
    }

    func hidePlaceholder() {
        imageView.backgroundColor = .clear
    }

    func updateImage(image: UIImage?) {
        guard let image = image else {
            setPlaceholder()
            return
        }
        imageView.image = image
    }

    private func setup() {
        setupConstraints()

        clipsToBounds = true
        isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFill

        setPlaceholder()
    }

    private func setupConstraints() {

        addSubview(imageView)

        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

    func reset() {
        imageView.image = nil
    }
}
