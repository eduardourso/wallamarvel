//
//  AppDelegate.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let mainNavigator: MainNavigator
    
    override convenience init() {
        let navigator = MainNavigator()
        self.init(navigator: navigator)
    }
    
    init(navigator: MainNavigator) {
        self.mainNavigator = navigator
        super.init()
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        guard let window = window else {
            return
        }
        
        mainNavigator.toStart(inWindow: window)
    }
}

