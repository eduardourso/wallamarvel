// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum CharacterDetailView {
    /// Detail
    internal static let title = L10n.tr("Base", "characterDetailView.title")
    internal enum Sections {
      /// Comics
      internal static let comics = L10n.tr("Base", "characterDetailView.sections.comics")
      /// Events
      internal static let events = L10n.tr("Base", "characterDetailView.sections.events")
      /// Stories
      internal static let stories = L10n.tr("Base", "characterDetailView.sections.stories")
    }
  }

  internal enum ErrorAlert {
    /// OK
    internal static let buttonTitle = L10n.tr("Base", "errorAlert.buttonTitle")
    /// Error
    internal static let title = L10n.tr("Base", "errorAlert.title")
  }

  internal enum IndexView {
    /// Marvel
    internal static let title = L10n.tr("Base", "indexView.title")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
