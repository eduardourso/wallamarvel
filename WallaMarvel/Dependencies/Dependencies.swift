//
//  Dependencies.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 22.08.19.
//

import Foundation
import Core

struct Dependencies {
    static let sharedImageCache = ImageCache()
    
    struct DataSources {
        static let indexDataSource = MarvelIndexDataSource(apiService: HTTPAPIService())
        static let characterDetailDataSource = MarvelCharacterDetailDataSource(apiService: HTTPAPIService())
    }
}
