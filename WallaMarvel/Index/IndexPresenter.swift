//
//  IndexPresenter.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation
import RxSwift
import Core

protocol IndexNavigator {
    func toCharacterDetail(character: Character)
}

protocol IndexDisplayer: class {
    func startLoading()
    func startLoadingNextPage()
    func stopLoading()
    func update(with characters: CharacterList)
    func append(characters: CharacterList)
    func update(with error: Error)
    func attachListener(_ listener: IndexListener)
    func detachListener()
}

protocol IndexListener {
    func fetchInitialCharacters()
    func fetchCharacters(with searchTerm: String?)
    func toCharacterDetail(character: Character)
    func fetchNextPage()
}

class IndexPresenter {
    
    private let indexService: IndexServiceProtocol
    private let navigator: IndexNavigator
    private var disposeBag = DisposeBag()
    private let observeScheduler: SchedulerType
    private weak var displayer: IndexDisplayer?
    private var searchTerm: String?
    private var subscription: Disposable?
    private var offset = 0
    private var hasMorePages = true
    
    init(indexService: IndexServiceProtocol,
         navigator: IndexNavigator,
         observeScheduler: SchedulerType = MainScheduler.instance,
         displayer: IndexDisplayer) {
        self.indexService = indexService
        self.navigator = navigator
        self.observeScheduler = observeScheduler
        self.displayer = displayer
    }
    
    func startPresenting() {
        displayer?.attachListener(self)
        fetchInitialCharacters()
    }
    
    func stopPresenting() {
        displayer?.detachListener()
        disposeBag = DisposeBag()
    }
}

extension IndexPresenter: IndexListener {
    func fetchInitialCharacters() {
        searchTerm = nil
        fetchCharactersFromOffsetZero()
    }
    
    func fetchCharacters(with searchTerm: String?) {
        self.searchTerm = searchTerm
        fetchCharactersFromOffsetZero()
    }
    
    private func fetchCharactersFromOffsetZero() {
        offset = 0
        hasMorePages = true
        
        displayer?.startLoading()
        subscription?.dispose()
        
        subscription = indexService.fetchCharacters(with: searchTerm, offset: offset)
            .observeOn(observeScheduler)
            .subscribe(onNext: updateDisplayer,
                       onError: updateOnError)
        
        subscription?.disposed(by: disposeBag)
    }
    
    func fetchNextPage() {
        guard hasMorePages else { return }
        displayer?.startLoadingNextPage()
        subscription?.dispose()
        
        subscription = indexService.fetchCharacters(with: searchTerm, offset: offset)
            .observeOn(observeScheduler)
            .subscribe(onNext: updateDisplayerAppending,
                       onError: updateOnError)
        
        subscription?.disposed(by: disposeBag)
    }
    
    private func updateDisplayer(characters: CharacterList) {
        displayer?.update(with: characters)
        displayer?.stopLoading()
        
        offset += 20
        hasMorePages = self.offset < characters.total
    }
    
    private func updateDisplayerAppending(characters: CharacterList) {
        displayer?.append(characters: characters)
        displayer?.stopLoading()
        
        offset += 20
        hasMorePages = self.offset < characters.total
    }
    
    private func updateOnError(_ error: Error) {
        displayer?.update(with: error)
        displayer?.stopLoading()
    }
    
    func toCharacterDetail(character: Character) {
        navigator.toCharacterDetail(character: character)
    }
}

