//
//  CharacterCell.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 22.08.19.
//

import UIKit
import Core

class CharacterCell: UITableViewCell {
    static let reuseIdentifier = "CharacterCell"
    
    private var characterView = CharacterCellView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(characterView)
        characterView.translatesAutoresizingMaskIntoConstraints = false
        characterView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        characterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        characterView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        characterView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
    }
    
    func update(with character: Character) {
        characterView.update(with: character)
    }
}
