//
//  CharacterCellView.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 24.08.19.
//

import UIKit
import Core

class CharacterCellView: UIView {
    private var urlImageView = URLImageView()
    private let cardView = UIView()
    private let nameLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init(imageView: URLImageView) {
        self.init()
        self.urlImageView = imageView
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        addSubview(cardView)
        cardView.addSubview(urlImageView)
        
        cardView.translatesAutoresizingMaskIntoConstraints = false
        cardView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        cardView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        cardView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        cardView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        cardView.layer.cornerRadius = 15
        cardView.layer.masksToBounds = true
        cardView.backgroundColor = .systemBackground
        
        urlImageView.translatesAutoresizingMaskIntoConstraints = false
        urlImageView.topAnchor.constraint(equalTo: cardView.topAnchor).isActive = true
        urlImageView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor).isActive = true
        urlImageView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor).isActive = true
        urlImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        urlImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        let textContainer = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel])
        textContainer.distribution = .fillProportionally
        textContainer.axis = .vertical
        cardView.addSubview(textContainer)
        
        textContainer.translatesAutoresizingMaskIntoConstraints = false
        textContainer.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 20).isActive = true
        textContainer.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -20).isActive = true
        textContainer.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -20).isActive = true
        textContainer.leadingAnchor.constraint(equalTo: urlImageView.trailingAnchor, constant: 20).isActive = true
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.textAlignment = .left
        nameLabel.numberOfLines = 2
        nameLabel.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textAlignment = .left
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont.italicSystemFont(ofSize: 12)
    }
    
    func update(with character: Character) {
        nameLabel.text = character.name
        descriptionLabel.text = character.description
        urlImageView.update(with: character.thumbnail.thumbnailURL())
    }
}

