//
//  IndexService.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import Foundation
import RxSwift
import Core

public protocol IndexServiceProtocol {
    func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList>
}

public class IndexService: IndexServiceProtocol {
    private let indexDataSource: IndexDataSource
    
    public init(indexDataSource: IndexDataSource) {
        self.indexDataSource = indexDataSource
    }
    
    public func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList> {
        return indexDataSource.fetchCharacters(with: searchTerm, offset: offset)
    }
}


