//
//  ViewController.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 21.08.19.
//

import UIKit
import Core

class IndexViewController: UIViewController {
    
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private var presenter: IndexPresenter?
    private var characters = [Character]()
    private let searchController = UISearchController(searchResultsController: nil)
    private var searchTask: DispatchWorkItem?
    private var listener: IndexListener?
    private let loadingIndicator = CustomLoadingIndicator()
    private let loadingFooter = LoadingTableFooterView()
    
    init(service: IndexService, navigator: IndexNavigator) {
        super.init(nibName: nil, bundle: nil)
        self.presenter = IndexPresenter(indexService: service, navigator: navigator, displayer: self)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        view.addSubview(tableView)
        view.addSubview(loadingIndicator)
        
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = ColorName.marvelRed.color
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        tableView.estimatedRowHeight = 240
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CharacterCell.self, forCellReuseIdentifier: CharacterCell.reuseIdentifier)
        tableView.keyboardDismissMode = .onDrag
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.IndexView.title
        presenter?.startPresenting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    deinit {
        presenter?.stopPresenting()
    }
}

extension IndexViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:  CharacterCell.reuseIdentifier, for: indexPath) as? CharacterCell else {
            preconditionFailure("Cell type \(CharacterCell.reuseIdentifier) could not be dequeued")
        }
        let character = characters[indexPath.row]
        cell.update(with: character)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let character = characters[indexPath.row]
        listener?.toCharacterDetail(character: character)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == characters.count - 1 {
            self.listener?.fetchNextPage()
        }
    }
}

extension IndexViewController: IndexDisplayer {
    func startLoading() {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func startLoadingNextPage() {
        tableView.tableFooterView = loadingFooter
        loadingFooter.startAnimating()
    }
    
    func stopLoading() {
        tableView.tableFooterView = nil
        loadingFooter.stopAnimating()
        loadingIndicator.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func update(with characters: CharacterList) {
        self.characters = characters.results
        tableView.reloadData()
    }
    
    func append(characters: CharacterList) {
        self.characters.append(contentsOf: characters.results)
        tableView.reloadData()
    }
    
    func update(with error: Error) {
        let alertController = UIAlertController(title: L10n.ErrorAlert.title, message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: L10n.ErrorAlert.buttonTitle, style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func attachListener(_ listener: IndexListener) {
        self.listener = listener
    }
    
    func detachListener() {
        self.listener = nil
    }
}

extension IndexViewController: UISearchBarDelegate {
    
    func filterContentForSearchText(_ searchText: String?) {
        self.searchTask?.cancel()
        
        guard let text = searchText, text.count > 0 else {
            self.listener?.fetchInitialCharacters()
            return
        }
        
        let task = DispatchWorkItem { [weak self] in
            self?.listener?.fetchCharacters(with: text)
        }
        self.searchTask = task
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: task)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchText = searchController.searchBar.text ?? ""
        filterContentForSearchText(searchText)
    }
}
