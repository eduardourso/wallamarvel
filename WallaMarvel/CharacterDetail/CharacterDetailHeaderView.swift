//
//  CharacterDetailHeaderView.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import UIKit
import Core

class CharacterDetailHeaderView: UIView {
    private var urlImageView = URLImageView()
    private let nameLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init(imageView: URLImageView) {
        self.init()
        self.urlImageView = imageView
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .systemBackground
        
        addSubview(urlImageView)
        
        urlImageView.translatesAutoresizingMaskIntoConstraints = false
        urlImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        urlImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        urlImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        let textContainer = UIStackView(arrangedSubviews: [nameLabel, descriptionLabel])
        textContainer.distribution = .fillProportionally
        textContainer.axis = .vertical
        addSubview(textContainer)
        
        textContainer.translatesAutoresizingMaskIntoConstraints = false
        textContainer.topAnchor.constraint(equalTo: urlImageView.bottomAnchor, constant: 20).isActive = true
        textContainer.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        textContainer.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        textContainer.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        
        let height = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        
        urlImageView.heightAnchor.constraint(equalToConstant: height * 0.8).isActive = true
        urlImageView.layer.masksToBounds = true
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.textAlignment = .left
        nameLabel.numberOfLines = 2
        nameLabel.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
        
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textAlignment = .left
        descriptionLabel.numberOfLines = 0
        descriptionLabel.font = UIFont.italicSystemFont(ofSize: 12)
    }
    
    func update(with character: Character) {
        nameLabel.text = character.name
        descriptionLabel.text = character.description
        urlImageView.update(with: character.thumbnail.thumbnailURL())
    }
}
