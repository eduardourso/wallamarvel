//
//  CharacterDetailHeaderCell.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import UIKit
import Core

class CharacterDetailHeaderCell: UITableViewCell {
    static let reuseIdentifier = "CharacterDetailHeaderCell"
    private let characterView = CharacterDetailHeaderView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        selectionStyle = .none
        contentView.addSubview(characterView)
        
        characterView.translatesAutoresizingMaskIntoConstraints = false
        characterView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        characterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        characterView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        characterView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
    }
    
    func update(with character: Character) {
        characterView.update(with: character)
    }
}
