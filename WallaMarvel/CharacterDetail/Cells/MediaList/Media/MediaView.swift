//
//  MediaView.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import UIKit
import Core

class MediaView: UIView {
    
    private var urlImageView = URLImageView()
    private let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    convenience init(imageView: URLImageView) {
        self.init()
        self.urlImageView = imageView
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        addSubview(urlImageView)
        addSubview(titleLabel)
        
        backgroundColor = .systemBackground
        
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        
        urlImageView.translatesAutoresizingMaskIntoConstraints = false
        urlImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        urlImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        urlImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: urlImageView.bottomAnchor, constant: 10).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    func update(with media: Media) {
        titleLabel.text = media.title
        urlImageView.update(with: media.thumbnail?.thumbnailURL())
    }
    
    func prepareForReuse() {
        urlImageView.reset()
    }
}
