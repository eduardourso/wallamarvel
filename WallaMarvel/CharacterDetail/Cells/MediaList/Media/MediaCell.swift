//
//  MediaCell.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 24.08.19.
//

import UIKit
import Core

class MediaCell: UICollectionViewCell {
    static let reuseIdentifier = "MediaCell"
    private let comicView = MediaView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        contentView.addSubview(comicView)
        
        comicView.translatesAutoresizingMaskIntoConstraints = false
        comicView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        comicView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        comicView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        comicView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    func update(with comic: Media) {
        comicView.update(with: comic)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        comicView.prepareForReuse()
    }
}
