
//
//  CharacterDetailPresenter.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import Foundation
import Core
import RxSwift

struct CharacterDetailViewState {
    let character: Character
    let comics: MediaList
    let series: MediaList
    let events: MediaList
    
    func sectionList() -> [CharacterDetailViewSections] {
        var sectionList: [CharacterDetailViewSections] = [.header(character: character)]
        if comics.results.count > 0 {
            sectionList.append(.comics(comics: comics.results))
        }
        
        if series.results.count > 0 {
            sectionList.append(.series(series: series.results))
        }
        
        if events.results.count > 0 {
            sectionList.append(.events(events: events.results))
        }
        return sectionList
    }
}

enum CharacterDetailViewSections {
    case header(character: Character)
    case comics(comics: [Media])
    case series(series: [Media])
    case events(events: [Media])
}

protocol CharacterDetailDisplayer: class {
    func startLoading()
    func stopLoading()
    func update(with sections: [CharacterDetailViewSections])
    func update(with error: Error)
}

class CharacterDetailPresenter {
    private let service: CharacterDetailServiceProtocol
    private weak var displayer: CharacterDetailDisplayer?
    private let character: Character
    private var disposeBag = DisposeBag()
    private let observeScheduler: SchedulerType
    
    init(service: CharacterDetailServiceProtocol, character: Character, observeScheduler: SchedulerType = MainScheduler.instance, displayer: CharacterDetailDisplayer) {
        self.service = service
        self.character = character
        self.observeScheduler = observeScheduler
        self.displayer = displayer
    }
    
    func startPresenting() {
        displayer?.startLoading()
        Observable.zip(Observable.just(character),
                       service.fetchComics(for: character.id),
                       service.fetchSeries(for: character.id),
                       service.fetchEvents(for: character.id),
                       resultSelector: CharacterDetailViewState.init)
            .observeOn(observeScheduler)
            .subscribe(onNext: { [weak self] viewState in
                self?.displayer?.stopLoading()
                self?.displayer?.update(with: viewState.sectionList())
            }, onError: { [weak self]  error in
                self?.displayer?.update(with: error)
                self?.displayer?.stopLoading()
            }).disposed(by: disposeBag)
    }
    
    func stopPresenting() {
        disposeBag = DisposeBag()
    }
}
