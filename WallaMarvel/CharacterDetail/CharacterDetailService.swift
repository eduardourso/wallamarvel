//
//  CharacterDetailService.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import Foundation
import RxSwift
import Core

public protocol CharacterDetailServiceProtocol {
    func fetchComics(for characterID: Int) -> Observable<MediaList>
    func fetchSeries(for characterID: Int) -> Observable<MediaList>
    func fetchEvents(for characterID: Int) -> Observable<MediaList>
}

public class CharacterDetailService: CharacterDetailServiceProtocol {
    private let dataSource: CharacterDetailDataSource
    
    public init(dataSource: CharacterDetailDataSource) {
        self.dataSource = dataSource
    }
    
    public func fetchComics(for characterID: Int) -> Observable<MediaList> {
        return dataSource.fetchComics(for: characterID)
    }
    
    public func fetchSeries(for characterID: Int) -> Observable<MediaList> {
        return dataSource.fetchSeries(for: characterID)
    }
    
    public func fetchEvents(for characterID: Int) -> Observable<MediaList> {
        return dataSource.fetchEvents(for: characterID)
    }
}
