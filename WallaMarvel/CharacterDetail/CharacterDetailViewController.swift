//
//  CharacterDetailViewController.swift
//  WallaMarvel
//
//  Created by Eduardo Urso on 23.08.19.
//

import UIKit
import Core

class CharacterDetailViewController: UIViewController {
    private let tableView = UITableView()
    private var presenter: CharacterDetailPresenter?
    private var sections = [CharacterDetailViewSections]()
    private let loadingIndicator = UIActivityIndicatorView(style: .large)
    
    init(service: CharacterDetailService, character: Character) {
        super.init(nibName: nil, bundle: nil)
        self.presenter = CharacterDetailPresenter(service: service, character: character, displayer: self)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        view.addSubview(tableView)
        view.addSubview(loadingIndicator)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.estimatedRowHeight = 260
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(CharacterDetailHeaderCell.self, forCellReuseIdentifier: CharacterDetailHeaderCell.reuseIdentifier)
        tableView.register(MediaListCell.self, forCellReuseIdentifier: MediaListCell.reuseIdentifier)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.CharacterDetailView.title
        presenter?.startPresenting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    deinit {
        presenter?.stopPresenting()
    }
}

extension CharacterDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = sections[indexPath.section]
        switch row {
        case .header(let character):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CharacterDetailHeaderCell.reuseIdentifier, for: indexPath) as? CharacterDetailHeaderCell else {
                preconditionFailure("Cell type \(CharacterDetailHeaderCell.reuseIdentifier) could not be dequeued")
            }
            cell.update(with: character)
            return cell
            
        case .comics(let comics):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaListCell.reuseIdentifier, for: indexPath) as? MediaListCell else {
                preconditionFailure("Cell type \(MediaListCell.reuseIdentifier) could not be dequeued")
            }
            cell.update(with: comics)
            return cell
            
        case .series(let series):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaListCell.reuseIdentifier, for: indexPath) as? MediaListCell else {
                preconditionFailure("Cell type \(MediaListCell.reuseIdentifier) could not be dequeued")
            }
            cell.update(with: series)
            return cell
        case .events(let events):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MediaListCell.reuseIdentifier, for: indexPath) as? MediaListCell else {
                preconditionFailure("Cell type \(MediaListCell.reuseIdentifier) could not be dequeued")
            }
            cell.update(with: events)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let row = sections[section]
        switch row {
        case .events:
            return L10n.CharacterDetailView.Sections.events
        case .comics:
            return L10n.CharacterDetailView.Sections.comics
        case .series:
            return L10n.CharacterDetailView.Sections.stories
        default:
            return nil
        }
    }
}

extension CharacterDetailViewController: CharacterDetailDisplayer {
    func update(with error: Error) {
        let alertController = UIAlertController(title: L10n.ErrorAlert.title, message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: L10n.ErrorAlert.buttonTitle, style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func startLoading() {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func stopLoading() {
        loadingIndicator.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func update(with sections: [CharacterDetailViewSections]) {
        self.sections = sections
        tableView.reloadData()
    }
}
