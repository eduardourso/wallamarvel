//
//  CharacterDetailDataSourceTests.swift
//  CoreTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import XCTest
import RxSwift

@testable import Core

class CharacterDetailDataSourceTests: XCTestCase {
    private var dataSource: MarvelCharacterDetailDataSource!
    private var service = MockAPIService()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        dataSource = MarvelCharacterDetailDataSource(apiService: service)
    }
    
    func testThatRequestBaseURLIsCorrectForCharacterComics() {
        _ = dataSource.fetchComics(for: 1234)
        XCTAssertEqual(service.request?.baseURL.description, "https://gateway.marvel.com")
    }
    
    func testThatRequestBaseURLIsCorrectForCharacterEvents() {
        _ = dataSource.fetchEvents(for: 1234)
        XCTAssertEqual(service.request?.baseURL.description, "https://gateway.marvel.com")
    }
    
    func testThatRequestBaseURLIsCorrectForCharacterSeries() {
        _ = dataSource.fetchSeries(for: 1234)
        XCTAssertEqual(service.request?.baseURL.description, "https://gateway.marvel.com")
    }
    
    func testThatRequestPathIsCorrectForCharacterComics() {
        _ = dataSource.fetchComics(for: 1234)
        XCTAssertEqual(service.request?.path.description, "/v1/public/characters/1234/comics")
    }
    
    func testThatRequestPathIsCorrectForCharacterEvents() {
        _ = dataSource.fetchEvents(for: 1234)
        XCTAssertEqual(service.request?.path.description, "/v1/public/characters/1234/events")
    }
    
    func testThatRequestPathIsCorrectForCharacterSeries() {
        _ = dataSource.fetchSeries(for: 1234)
        XCTAssertEqual(service.request?.path.description, "/v1/public/characters/1234/series")
    }
}
