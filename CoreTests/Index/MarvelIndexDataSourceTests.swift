//
//  MarvelIndexDataSource.swift
//  CoreTests
//
//  Created by Eduardo Urso on 22.08.19.
//

import XCTest
import RxSwift

@testable import Core

class MarvelIndexDataSourceTests: XCTestCase {
    private var dataSource: MarvelIndexDataSource!
    private var service = MockAPIService()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        dataSource = MarvelIndexDataSource(apiService: service)
    }
    
    func testThatRequestBaseURLIsCorrectForCharacters() {
        _ = dataSource.fetchCharacters(with: nil, offset: 0)
        XCTAssertEqual(service.request?.baseURL.description, "https://gateway.marvel.com")
    }
    
    func testThatRequestPathIsCorrectForCharacters() {
        _ = dataSource.fetchCharacters(with: "Ironman", offset: 0)
        XCTAssertEqual(service.request?.path.description, "/v1/public/characters")
    }
    
    func testThatRequestQueryItemsKeysIsCorrectForCharacters() {
        _ = dataSource.fetchCharacters(with: nil, offset: 0)
        let expectedQueryItemsKeys = ["apikey", "ts", "hash", "offset"]
        let keys = service.request?.queryItems?.map({ return $0.name })
        XCTAssertEqual(keys, expectedQueryItemsKeys)
    }
    
    func testThatRequestQueryItemsKeysIsCorrectWhenSearchingForCharacters() {
        _ = dataSource.fetchCharacters(with: "Ironman", offset: 0)
        let expectedQueryItemsKeys = ["apikey", "ts", "hash", "offset", "nameStartsWith"]
        let keys = service.request?.queryItems?.map({ return $0.name })
        XCTAssertEqual(keys, expectedQueryItemsKeys)
    }
}


class MockAPIService: APIService {
    var request: APIRequest?
    
    func fetch<T>(request: APIRequest) -> Observable<T> where T : Decodable {
        self.request = request
        return Observable.empty()
    }
}
