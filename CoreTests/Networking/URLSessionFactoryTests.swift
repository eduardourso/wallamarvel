//
//  URLSessionFactoryTests.swift
//  CoreTests
//
//  Created by Eduardo Urso on 22.08.19.
//

import XCTest
@testable import Core

class URLSessionFactoryTests: XCTestCase {
    
    func testThatNetworkingPolicyIsProtocolCachePolicy() {
        let factory = makeSessionFactory()
        let session = factory.makeSession()
        
        XCTAssertEqual(session.configuration.requestCachePolicy, .useProtocolCachePolicy)
    }
}

extension URLSessionFactoryTests {
    fileprivate func makeSessionFactory() -> URLSessionFactory {
        let configuration = URLSessionConfiguration.default
        return URLSessionFactory(configuration: configuration)
    }
}
