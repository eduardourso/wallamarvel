//
//  MockIndexNavigator.swift
//  WallaMarvelTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import Foundation

@testable import WallaMarvel
@testable import Core

class MockNavigator: IndexNavigator {
    var toCharacterDetailFuncCalled = 0
    func toCharacterDetail(character: Character) {
        toCharacterDetailFuncCalled += 1
    }
}
