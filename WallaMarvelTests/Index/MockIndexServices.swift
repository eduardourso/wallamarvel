//
//  MockIndexServices.swift
//  WallaMarvelTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import Foundation
import RxSwift

@testable import WallaMarvel
@testable import Core

class SuccessIndexService: IndexServiceProtocol {
    func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList> {
        let characters = [Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))]
        let data = CharacterList(results: characters, total: 40, offset: 20)
        return Observable.from(optional: data)
    }
}

class SuccessButNoMorePageIndexService: IndexServiceProtocol {
    func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList> {
        let characters = [Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))]
        let data = CharacterList(results: characters, total: 10, offset: 0)
        return Observable.from(optional: data)
    }
}

struct MockError: Error {}

class FailureIndexService: IndexServiceProtocol {
    func fetchCharacters(with searchTerm: String?, offset: Int) -> Observable<CharacterList> {
        return Observable.error(MockError())
    }
    
    func fetchCharacters() -> Observable<CharacterList> {
        return Observable.error(MockError())
    }
}
