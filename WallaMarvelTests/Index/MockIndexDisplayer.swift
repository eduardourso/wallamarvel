//
//  MockIndexDisplayer.swift
//  WallaMarvelTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import Foundation
@testable import WallaMarvel
@testable import Core

class MockDisplayer: IndexDisplayer {
    var startLoadingNextPageCalled = 0
    func startLoadingNextPage() {
        startLoadingNextPageCalled += 1
    }
    
    var appendCharactersFuncCalled = 0
    func append(characters: CharacterList) {
        appendCharactersFuncCalled += 1
    }
    
    var updateWithErrorCalled = 0
    func update(with error: Error) {
        updateWithErrorCalled += 1
    }
    
    var startLoadingCalled = 0
    func startLoading() {
        startLoadingCalled += 1
    }
    
    var stopLoadingCalled = 0
    func stopLoading() {
        stopLoadingCalled += 1
    }
    
    var listener: IndexListener?
    
    var attachListenerCalled = 0
    func attachListener(_ listener: IndexListener) {
        self.listener = listener
        attachListenerCalled += 1
    }
    
    var detachListenerCalled = 0
    func detachListener() {
        detachListenerCalled += 1
    }
    
    var updateFuncCalled = 0
    func update(with characters: CharacterList) {
        updateFuncCalled += 1
    }
}
