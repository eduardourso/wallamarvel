//
//  IndexPresenterTests.swift
//  WallaMarvelTests
//
//  Created by Eduardo Urso on 22.08.19.
//

import XCTest

@testable import WallaMarvel
@testable import Core

class IndexPresenterTests: XCTestCase {
    private var presenter: IndexPresenter!
    private var displayer: MockDisplayer!
    private var navigator: MockNavigator!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.displayer = MockDisplayer()
        self.navigator = MockNavigator()
        self.presenter = IndexPresenter(indexService: SuccessIndexService(),
                                        navigator: navigator,
                                        displayer: displayer)
    }

    func testThatDisplayerIsUpdatedWithCharacterListOnStartPresenting() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.updateFuncCalled, 1)
    }
    
    func testThatListenerIsAttachedOnStartPresenting() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.attachListenerCalled, 1)
    }
    
    func testThatListenerIsDetachedOnStopPresenting() {
        self.presenter.startPresenting()
        self.presenter.stopPresenting()
        
        XCTAssertEqual(displayer.detachListenerCalled, 1)
    }
    
    func testThatPresenterNavigatesToCharacterDetail() {
        self.presenter.startPresenting()
        let character = Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))
        displayer.listener?.toCharacterDetail(character: character)
        
        XCTAssertEqual(navigator.toCharacterDetailFuncCalled, 1)
    }
    
    func testThatDisplayerStartsLoadingOnStartPresenting() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.startLoadingCalled, 1)
    }
    
    func testThatDisplayerStopsLoadingOnContentIsReceived() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.stopLoadingCalled, 1)
    }
    
    func testThatDisplayerStopsLoadingWhenErrorLoadingContentOccurs() {
        self.presenter = IndexPresenter(indexService: FailureIndexService(), navigator: navigator, displayer: displayer)
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.stopLoadingCalled, 1)
    }
    
    func testThatDisplayerUpdatesWithErrorWhenLoadingContentFails() {
        self.presenter = IndexPresenter(indexService: FailureIndexService(), navigator: navigator, displayer: displayer)
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.updateWithErrorCalled, 1)
    }
    
    func testThatDisplayerStartsLoadingNextPageWhenLoadingMorePages() {
        self.presenter.startPresenting()
        
        displayer.listener?.fetchNextPage()
        
        XCTAssertEqual(displayer.startLoadingNextPageCalled, 1)
    }
    
    func testThatDisplayerDoesntStartsLoadingNextPageWhenThereAreNoMorePages() {
        self.presenter = IndexPresenter(indexService: SuccessButNoMorePageIndexService(), navigator: navigator, displayer: displayer)
        self.presenter.startPresenting()
        
        displayer.listener?.fetchNextPage()
        
        XCTAssertEqual(displayer.startLoadingNextPageCalled, 0)
    }
    
    func testThatPresenterAppendsCharacterWhenLoadingMorePages() {
        self.presenter.startPresenting()
        
        displayer.listener?.fetchNextPage()
        
        XCTAssertEqual(displayer.appendCharactersFuncCalled, 1)
    }
}
