//
//  CharacterDetailPresenterTests.swift
//  WallaMarvelTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import XCTest
import RxSwift

@testable import WallaMarvel
@testable import Core

class CharacterDetailPresenterTests: XCTestCase {
    private var presenter: CharacterDetailPresenter!
    private var displayer: CharacterDetailMockDisplayer!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.displayer = CharacterDetailMockDisplayer()
         let character = Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))
        self.presenter = CharacterDetailPresenter(service: SuccessCharacterDetaiService(), character: character, displayer: displayer)
    }
    
    func testThatDisplayerIsUpdatedWithSectionsOnStartPresenting() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.updateWithSectionsCalled, 1)
    }
    
    func testThatDisplayerStartsLoadingOnStartPresenting() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.startLoadingCalled, 1)
    }
    
    func testThatDisplayerStopsLoadingOnContentIsReceived() {
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.stopLoadingCalled, 1)
    }
    
    func testThatDisplayerStopsLoadingWhenErrorLoadingContentOccurs() {
        let character = Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))
        self.presenter = CharacterDetailPresenter(service: FailureCharacterDetaiService(), character: character, displayer: displayer)
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.stopLoadingCalled, 1)
    }
    
    func testThatDisplayerUpdatesWithErrorWhenLoadingContentFails() {
        let character = Character(id: 0, name: "name", description: "description", resourceURI: "resourceURI", thumbnail: Thumbnail(fileExtension: "fileExtension", path: "path"))
        self.presenter = CharacterDetailPresenter(service: FailureCharacterDetaiService(), character: character, displayer: displayer)
        self.presenter.startPresenting()
        
        XCTAssertEqual(displayer.updateWithErrorCalled, 1)
    }
}

class SuccessCharacterDetaiService: CharacterDetailServiceProtocol {
    func fetchComics(for characterID: Int) -> Observable<MediaList> {
        let data = MediaList(results: [Media(id: 0, title: "title", thumbnail: nil)])
        return Observable.from(optional: data)
    }
    
    func fetchSeries(for characterID: Int) -> Observable<MediaList> {
        let data = MediaList(results: [Media(id: 0, title: "title", thumbnail: nil)])
        return Observable.from(optional: data)
    }
    
    func fetchEvents(for characterID: Int) -> Observable<MediaList> {
        let data = MediaList(results: [Media(id: 0, title: "title", thumbnail: nil)])
        return Observable.from(optional: data)
    }
}

class FailureCharacterDetaiService: CharacterDetailServiceProtocol {
    func fetchComics(for characterID: Int) -> Observable<MediaList> {
        return Observable.error(MockError())
    }
    
    func fetchEvents(for characterID: Int) -> Observable<MediaList> {
        return Observable.error(MockError())
    }
    
    func fetchSeries(for characterID: Int) -> Observable<MediaList> {
        return Observable.error(MockError())
    }
}

class CharacterDetailMockDisplayer: CharacterDetailDisplayer {
    var updateWithSectionsCalled = 0
    func update(with sections: [CharacterDetailViewSections]) {
        updateWithSectionsCalled += 1
    }
    
    var updateWithErrorCalled = 0
    func update(with error: Error) {
        updateWithErrorCalled += 1
    }
    
    var startLoadingCalled = 0
    func startLoading() {
        startLoadingCalled += 1
    }
    
    var stopLoadingCalled = 0
    func stopLoading() {
        stopLoadingCalled += 1
    }
}
