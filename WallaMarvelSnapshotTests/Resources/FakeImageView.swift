//
//  FakeImageView.swift
//  WallaMarvelSnapshotTests
//
//  Created by Eduardo Urso on 24.08.19.
//

import Foundation
import UIKit
@testable import WallaMarvel

class FakeImageView: URLImageView {
    override func update(with url: URL?) {
        do {
            guard let url = url else {
                preconditionFailure("No url specified in test")
            }
            
            let data = try Data(contentsOf: url)
            updateImage(image: UIImage(data: data))
        } catch {
            preconditionFailure("\(error)")
        }
    }
}
