//
//  CharacterDetailSnapshotTests.swift
//  WallaMarvelSnapshotTests
//
//  Created by Eduardo Urso on 24.08.19.
//
import FBSnapshotTestCase

@testable import WallaMarvel
@testable import Core

class CharacterDetailSnapshotTests: FBSnapshotTestCase {
    
    func pathToLocalImage() -> String {
        guard let url = Bundle(for: type(of: self)).url(forResource: "spider-man", withExtension: "jpg") else {
            preconditionFailure()
        }
        return url.deletingPathExtension().absoluteString
    }
    
    override func setUp() {
        super.setUp()
        recordMode = false
    }
    
    func testThatCharacterDetailHeaderCellIsCorrectlyCreated() {
        let view = CharacterDetailHeaderView(imageView: FakeImageView())
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 320).isActive = true
        
        let thumbnail = Thumbnail(fileExtension: "jpg", path: pathToLocalImage())
        let character = Character(id: 0, name: "Fake Spider-Man", description: "Long description description description description description description", resourceURI: "", thumbnail: thumbnail)
        
        view.update(with: character)
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        FBSnapshotVerifyView(view)
    }
    
    func testThatCharacterDetailHeaderCellIsCorrectlyCreatedWhenNoDescriptionIsProvided() {
        let view = CharacterDetailHeaderView(imageView: FakeImageView())
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: 320).isActive = true
        
        let thumbnail = Thumbnail(fileExtension: "jpg", path: pathToLocalImage())
        let character = Character(id: 0, name: "Fake Spider-Man", description: "", resourceURI: "", thumbnail: thumbnail)
        
        view.update(with: character)
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
        FBSnapshotVerifyView(view)
    }
    
}
